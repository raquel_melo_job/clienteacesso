package com.itau.GerenciadorAcesso.consumer;

import com.itau.GerenciadorAcesso.producer.Passe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class RespostaAcesso {

    @Autowired
    private EscreveArquivoLog escreveArquivoLog;

    @KafkaListener(topics = "spec2-raquel-batista-1",groupId = "grupo-Raquel")
    public void resposta(@Payload Passe passe){
        boolean resposta = passe.isAutoriza();
        escreveArquivoLog.escreve(passe);
    }
}
