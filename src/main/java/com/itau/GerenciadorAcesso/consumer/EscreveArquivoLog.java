package com.itau.GerenciadorAcesso.consumer;

import com.itau.GerenciadorAcesso.producer.Passe;
import org.springframework.stereotype.Component;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class EscreveArquivoLog {
    public void escreve(Passe passe) {
        try {
            String permissao = "Negado";
            if(passe.isAutoriza()){
                permissao = "Concedido";
            }
            FileWriter arq = new FileWriter("/home/a2/Documentos/Projetos/Mensageria/producer/src/main/java/br/com/mastertech/producer/log.txt",true);
            PrintWriter out = new PrintWriter(arq);
            out.printf("O acesso do cliente "+passe.getNomeCliente()+" foi "+permissao+"%n");
            arq.close();
            System.out.println("Escreveu o log com sucesso.");
        } catch (IOException e) {
            System.out.println("Aconteceu um erro na escrita do arquivo de log.");
            e.printStackTrace();
        }
    }
}
